#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ThingSpeak.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME680.h>

const char* WifiName="ESPWebServer";  // SSID of the ESP8266 upon booting for the first time
const char* WifiPass="12345678";      // Password of the ESP8266 hot spot
String webPage,notice,NewSSID,NewPassword;

ESP8266WebServer server(80);

IPAddress    apIP(42, 42, 42, 42);    // Default address of the webserver


const char htmlPage[]PROGMEM=R"=====(
<!DOCTYPE html>
<html>
<body>
<h3>Configure New SSID and Password </h3>
<FORM METHOD="POST"action="/newSSID">
<input type="text" name="SSID" value="SSID">
<input type="text" name="Password" value="Password">
<input type="submit" value="Post Notice">
</form>
</body>
</html>
)=====";


void configureNewWiFi(String ssid, String password)
{
  WiFi.mode(WIFI_STA);
  Serial.println();
  Serial.print("Connecting");
  WiFi.begin(ssid, password);
  while(WiFi.status()!= WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
    } 
 Serial.println("");
 Serial.println("Wi-Fi Connected");
 Serial.println("IP Address is:");
 Serial.println(WiFi.localIP());
}


void newSSID()
{
 webPage=htmlPage;
 // TODO: Make the previous configuration save in Flash and allow user to change the wifi creds or keep the old ones
 NewSSID =server.arg("SSID"); // Save Newly Entered SSID
 NewPassword = server.arg("Password");  // Save Newly Entered Password
 Serial.println("Text Received, contents:");
 Serial.print("SSID: ");
 Serial.println(NewSSID);
 Serial.print("Password: ");
 Serial.println(NewPassword);
 server.send(200,"text/html",webPage);
 // TODO: Land the user on a screen that confirms the connection to WiFi by pinging google or something
 if(NewSSID != "")
 {
   configureNewWiFi(NewSSID, NewPassword);
 }
}

void setup()
   {
  Serial.begin(115200); // Initialize Serial
  delay(10);            // Let Serial Init (probably not needed)
  WiFi.mode(WIFI_AP);   // Configure the ESP8266 to be a WiFi Access Point Only
  WiFi.softAP(WifiName, WifiPass);  // Set the SSID and Password for the Access Point
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0)); // Set the IP address for the webserver
  Serial.println();
  server.on("/newSSID",newSSID);  // Initialize the Webserver listening for connections over the newSSID webpage to perform the newSSID function
  server.begin(); // Begin the Webserver
  Serial.println("HTTP Server Started");
}


void loop() {
  server.handleClient();  // Wait for a client to connect and then handle if there is a client
}
