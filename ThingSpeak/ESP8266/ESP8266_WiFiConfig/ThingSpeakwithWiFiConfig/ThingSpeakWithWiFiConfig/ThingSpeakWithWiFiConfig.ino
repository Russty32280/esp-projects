#include "ThingSpeak.h"
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME680.h>


// assign the ESP8266 pins to arduino pins
#define D1 5
#define D2 4
#define D4 2
#define D3 0

// assign the SPI bus to pins
#define BME_SCK D1
#define BME_MISO D4
#define BME_MOSI D2
#define BME_CS D3

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME680 bme(BME_CS, BME_MOSI, BME_MISO, BME_SCK); // software SPI

unsigned long delayTime;




const char* WifiName="ESPWebServer";  // SSID of the ESP8266 upon booting for the first time
const char* WifiPass="12345678";      // Password of the ESP8266 hot spot
String webPage,notice,NewSSID,NewPassword;

ESP8266WebServer server(80);

IPAddress    apIP(42, 42, 42, 42);    // Default address of the webserver

int StartSensingFlag = 0;

const char htmlPage[]PROGMEM=R"=====(
<!DOCTYPE html>
<html>
<body>
<h3>Configure New SSID and Password </h3>
<FORM METHOD="POST"action="/newSSID">
<input type="text" name="SSID" value="SSID">
<input type="text" name="Password" value="Password">
<input type="submit" value="Post Notice">
</form>
</body>
</html>
)=====";


void configureNewWiFi(String ssid, String password)
{
  WiFi.mode(WIFI_STA);
  Serial.println();
  Serial.print("Connecting");
  WiFi.begin(ssid, password);
  while(WiFi.status()!= WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
    } 
 Serial.println("");
 Serial.println("Wi-Fi Connected");
 Serial.println("IP Address is:");
 Serial.println(WiFi.localIP());
 StartSensingFlag = 1;
}


void newSSID()
{
 webPage=htmlPage;
 // TODO: Make the previous configuration save in Flash and allow user to change the wifi creds or keep the old ones
 NewSSID =server.arg("SSID"); // Save Newly Entered SSID
 NewPassword = server.arg("Password");  // Save Newly Entered Password
 Serial.println("Text Received, contents:");
 Serial.print("SSID: ");
 Serial.println(NewSSID);
 Serial.print("Password: ");
 Serial.println(NewPassword);
 server.send(200,"text/html",webPage);
 // TODO: Land the user on a screen that confirms the connection to WiFi by pinging google or something
 if(NewSSID != "")
 {
   configureNewWiFi(NewSSID, NewPassword);
 }
}

void setup()
   {
  Serial.begin(115200); // Initialize Serial
  delay(10);            // Let Serial Init (probably not needed)
  WiFi.mode(WIFI_AP);   // Configure the ESP8266 to be a WiFi Access Point Only
  WiFi.softAP(WifiName, WifiPass);  // Set the SSID and Password for the Access Point
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0)); // Set the IP address for the webserver
  Serial.println();
  server.on("/newSSID",newSSID);  // Initialize the Webserver listening for connections over the newSSID webpage to perform the newSSID function
  server.begin(); // Begin the Webserver
  Serial.println("HTTP Server Started");
  Serial.println(F("BME680 test"));
  if (!bme.begin()) {
    Serial.println("Could not find a valid BME680 sensor, check wiring!");
    while (1);
  }
  // Set up oversampling and filter initialization
  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320, 150); // 320*C for 150 ms
}


void loop() {
  if(StartSensingFlag == 0)
  {
    server.handleClient();  // Wait for a client to connect and then handle if there is a client
  }
  else
  {
     Serial.print("Temperature = ");
    float Temperature = bme.readTemperature();
    Serial.print(Temperature);
    Serial.println(" *C");

    Serial.print("Pressure = ");
    float Pressure = bme.readPressure() / 100.0F;
    Serial.print(Pressure);
    Serial.println(" hPa");

    Serial.print("Approx. Altitude = ");
    float Altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);
    Serial.print(Altitude);
    Serial.println(" m");

    Serial.print("Humidity = ");
    float Humidity = bme.readHumidity();
    Serial.print(Humidity);
    Serial.println(" %");

    Serial.println();
  
  // Write to ThingSpeak. There are up to 8 fields in a channel, allowing you to store up to 8 different
  // pieces of information in a channel.  Here, we write to field 1.

    int x = ThingSpeak.setField(1, Temperature);
    if(x == 200){
      Serial.println("Channel update successful.");
    }
    else{
      Serial.println("Problem setting Field 1. HTTP error code " + String(x));
    }


    x = ThingSpeak.setField(2, Humidity);
    if(x == 200){
      Serial.println("Channel update successful.");
    }
  else{
    Serial.println("Problem setting Field 2. HTTP error code " + String(x));
  }

  x = ThingSpeak.setField(3, Pressure);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem setting Field 3. HTTP error code " + String(x));
  }

  x = ThingSpeak.setField(4, Altitude);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem setting Field 4. HTTP error code " + String(x));
  }


  
  x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem updating channel. HTTP error code " + String(x));
  }

  //ESP.deepSleep(20e6);
  delay(15000); // Wait 20 seconds to update the channel again
  }
}
