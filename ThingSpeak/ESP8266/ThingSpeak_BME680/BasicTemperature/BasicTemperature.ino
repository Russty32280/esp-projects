#include "ThingSpeak.h"
#include "secrets.h"
#include <ESP8266WiFi.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME680.h>


char ssid[] = SECRET_SSID;   // your network SSID (name) 
char pass[] = SECRET_PASS;   // your network password
int keyIndex = 0;            // your network key Index number (needed only for WEP)
WiFiClient  client;

unsigned long myChannelNumber = SECRET_CH_ID;
const char * myWriteAPIKey = SECRET_WRITE_APIKEY;

int number = 0;

// assign the ESP8266 pins to arduino pins
#define D1 5
#define D2 4
#define D4 2
#define D3 0

// assign the SPI bus to pins
#define BME_SCK D1
#define BME_MISO D4
#define BME_MOSI D2
#define BME_CS D3

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME680 bme(BME_CS, BME_MOSI, BME_MISO, BME_SCK); // software SPI

unsigned long delayTime;




void setup() {
  Serial.begin(115200);  // Initialize serial

  WiFi.mode(WIFI_STA); 
  ThingSpeak.begin(client);  // Initialize ThingSpeak

  Serial.println(F("BME680 test"));

  bool status;
  
  // default settings
  status = bme.begin();
  if (!status) {
      Serial.println("Could not find a valid BME680 sensor, check wiring!");
      while (1);
  }
  
  Serial.println("-- Default Test --");
  delayTime = 1000;

  Serial.println();
  
}

void loop() {

  // Connect or reconnect to WiFi
  if(WiFi.status() != WL_CONNECTED){
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(SECRET_SSID);
    while(WiFi.status() != WL_CONNECTED){
      WiFi.begin(ssid, pass);  // Connect to WPA/WPA2 network. Change this line if using open or WEP network
      Serial.print(".");
      delay(5000);     
    } 
    Serial.println("\nConnected.");
  }

  
  Serial.print("Temperature = ");
  float Temperature = bme.readTemperature();
  Serial.print(Temperature);
  Serial.println(" *C");

  Serial.print("Pressure = ");
  float Pressure = bme.readPressure() / 100.0F;
  Serial.print(Pressure);
  Serial.println(" hPa");

  Serial.print("Approx. Altitude = ");
  float Altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);
  Serial.print(Altitude);
  Serial.println(" m");

  Serial.print("Humidity = ");
  float Humidity = bme.readHumidity();
  Serial.print(Humidity);
  Serial.println(" %");

  Serial.println();
  
  // Write to ThingSpeak. There are up to 8 fields in a channel, allowing you to store up to 8 different
  // pieces of information in a channel.  Here, we write to field 1.

  int x = ThingSpeak.setField(1, Temperature);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem setting Field 1. HTTP error code " + String(x));
  }


  x = ThingSpeak.setField(2, Humidity);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem setting Field 2. HTTP error code " + String(x));
  }

  x = ThingSpeak.setField(3, Pressure);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem setting Field 3. HTTP error code " + String(x));
  }

  x = ThingSpeak.setField(4, Altitude);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem setting Field 4. HTTP error code " + String(x));
  }


  
  x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem updating channel. HTTP error code " + String(x));
  }

  //ESP.deepSleep(20e6);
  delay(15000); // Wait 20 seconds to update the channel again
}
