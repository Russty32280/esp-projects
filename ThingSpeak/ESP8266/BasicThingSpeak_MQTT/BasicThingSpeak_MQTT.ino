#include <ESP8266WiFi.h>
#include <PubSubClient.h>
 
const char* ssid = "Rowan_IoT";
const char* password =  "";
const char* mqttServer = "mqtt.thingspeak.com";
const int mqttPort = 1883;
const char* mqttUser = "ESP8266_1";
const char* mqttPassword = "T4CQPGL9GYAVIQBR";

const char* ThingSpeak_WriteAPI = "T4CQPGL9GYAVIQBR";
const char* ThingSpeak_ChannelID = "735937";


const char* ThingSpeak_GlobalTopic = "channels/735937/publish/T4CQPGL9GYAVIQBR";
const char* ThingSpeak_Field1Topic = "channels/735937/publish/fields/field1/T4CQPGL9GYAVIQBR";
const char* ThingSpeak_Field2Topic = "channels/735937/publish/fields/field2/T4CQPGL9GYAVIQBR";


//const char* ThingSpeak_GlobalTopic = "channels/" + ThingSpeak_ChannelID + "/publish/" + ThingSpeak_WriteAPI;
//const char* ThingSpeak_Field1Topic = "channels/" + ThingSpeak_ChannelID + "/publish/fields/field1/" + ThingSpeak_WriteAPI;
//const char* ThingSpeak_Field2Topic = "channels/" + ThingSpeak_ChannelID + "/publish/fields/field2/" + ThingSpeak_WriteAPI;

long RandomTemp = 0;
//char* TempString = String(RandomTemp).c_str();
 
WiFiClient espClient;
PubSubClient client(espClient);
 
void setup() {
 
  Serial.begin(115200);
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
 
  Serial.println("Connected to the WiFi network");
 
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
 
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
 
    if (client.connect("ESP32Client", mqttUser, mqttPassword )) {
 
      Serial.println("connected");
 
    } else {
 
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
 
    }
  }
 
  //client.publish("esp/test", "Hello from ESP32");
  //client.publish("esp/temp", "0");
  //client.subscribe("esp/test");
  //client.subscribe("channels/735937/subscribe/fields/field1/39DQTAW7M68GTD0M");
}

void callback(char* topic, byte* payload, unsigned int length) {
 
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
 
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
 
  Serial.println();
  Serial.println("-----------------------");
 
}
 
void loop() {
  RandomTemp = random(0,100);
  //Serial.println(RandomTemp);
  if (RandomTemp > 0)
  {
    String TempString = String(RandomTemp);
    char TempCharArray[10];
    TempString.toCharArray(TempCharArray, sizeof(TempString));
    client.publish(ThingSpeak_Field1Topic, TempCharArray);
    Serial.println(TempCharArray);
  }
  delay(15000);
  client.loop();
}
