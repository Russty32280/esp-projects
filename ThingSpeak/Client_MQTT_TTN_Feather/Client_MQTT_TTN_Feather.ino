#include <Narcoleptic.h>
#include <avr/wdt.h>
#include <DHT.h>
#include <SPI.h>
#include <RH_RF95.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <NewPing.h>
#include <Battery.h>
//#include "LowPower.h"



// Singleton instance of the radio driver
RH_RF95 rf95;
float frequency = 915.0;  //frequency settings

#define DHT11_PIN 5

#define trigPin 2
#define echoPin 3

#define MAX_DISTANCE 350

float temperature, humidity, tem, hum, duration, distance, voltage, level;
char tem_1[8]={"\0"},hum_1[8]={"\0"};
char dist_1[8]={"\0"};
char gps_lon[20]={"\0"};  
char gps_lat[20]={"\0"};
char volt_1 [8] = {"\0"}, lev_1{"\0"}; 
String datastring="";
String datastring1="";
char *node_id = "<12345>";  //From LG01 via web Local Channel settings on MQTT.Please refer <> dataformat in here.
uint8_t datasend[64];
unsigned int count = 1;

//for GPS
static const int RXPin = 4, TXPin = 11;
static const uint32_t GPSBaud = 9600;
float flon, flat;


TinyGPSPlus gps;

NewPing sonar(trigPin, echoPin, MAX_DISTANCE);

SoftwareSerial ss(RXPin, TXPin);

Battery battery(3100, 9000, A0);

unsigned long last = 0UL;


DHT dht(DHT11_PIN, DHT11);

void setup()
{
      // ATmega48/88/168/328
      DIDR0 = B00111111; //Disable digital input buffers on all ADC0-ADC5 pins
      DIDR1 = (1<<AIN1D)|(1<<AIN0D); //Disable digital input buffer on AIN1/0
      
      Serial.begin(115200);
      ss.begin(GPSBaud);
      dht.begin();
      battery.begin(5000, 1.5);
      Serial.println(F("Start MQTT Example"));
          if (!rf95.init())
      Serial.println(F("init failed"));
         rf95.setFrequency(frequency);
         rf95.setTxPower(13);
         rf95.setSyncWord(0x34);
         rf95.setSpreadingFactor(8);
         rf95.setSignalBandwidth(41700);
         rf95.setCodingRate4(5);
      //ss.println("Hello");
      Serial.println(rf95.sleep());
}

//Read data from sensor and store in local variables
void dhtTem()
{
       tem = dht.readTemperature();    //Read Tmperature data
       hum = dht.readHumidity();      //Read humidity data      
       Serial.println(F("The temperature and humidity:"));
       Serial.print("[");
       Serial.print(tem);
       Serial.print("℃");
       Serial.print(",");
       Serial.print(hum);
       Serial.print("%");
       Serial.print("]");
       Serial.println("");
}

//Combine all data into a single character array
//with field definitions and node id for gateway
void dataWrite()
{
    char data[100] = "\0";
    for(int i = 0; i < 100; i++)
    {
       data[i] = node_id[i];
    }

    dtostrf(tem,0,1,tem_1);
    dtostrf(hum,0,1,hum_1);
    

     //Serial.println(tem_1);
     /*strcat(data,"field1=");
     strcat(data,tem_1);
     strcat(data,"&field2=");
     strcat(data,hum_1);*/
     dtostrf(voltage, 0, 0, volt_1);
     dtostrf(level, 0, 0, lev_1);
     strcat(data,"field1=");
     strcat(data, volt_1);
     strcat(data,"&field2=");
     strcat(data, lev_1);
     
     if((flon < 1000.0) && (flat <1000.0) && (gps.location.isValid())){
      dtostrf(flon, 0, 4, gps_lon);
      dtostrf(flat, 0, 4, gps_lat);
      strcat(data,"&field3=");
      strcat(data, gps_lon);
      strcat(data,"&field4=");
      strcat(data, gps_lat);     
     }

     dtostrf(distance, 0, 1, dist_1);
     strcat(data,"&field5=");
     strcat(data, dist_1);

     strcpy((char *)datasend,data);
     
   //Serial.println((char *)datasend);
    //Serial.println(sizeof datasend);
      
}

//Transmit data out over LoRa
void SendData()
{
      Serial.println(F("Sending data to LG01:"));
      Serial.println((char*)datasend);
           
   
      rf95.send((char *)datasend,sizeof(datasend));  
      rf95.waitPacketSent();  // Now wait for a reply
    
      uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
      uint8_t len = sizeof(buf);

     if (rf95.waitAvailableTimeout(3000))
  { 
    // Should be a reply message for us now   
    if (rf95.recv(buf, &len))
   {
     
      Serial.print("got reply from LG01: ");
      Serial.println((char*)buf);
      Serial.print("RSSI: ");
      Serial.println(rf95.lastRssi(), DEC);    
    }
    else
    {
      Serial.println("recv failed");
    }
  }
  else
  {
    Serial.println("No reply, is LoRa server running?");
  }
  Serial.println(rf95.sleep());
}

void arduinoSleep(int seconds){
  delay(1000);//allow time for any previous serial transmissions to complete
  int count = 0;
  while(count < seconds){
    Narcoleptic.sleep(WDTO_8S);
    count+=8;
  }
}

//Read data from GPS
//*May have to run a few times in a loop before getting any valid data
void GPSread(){
  // Dispatch incoming characters
  
  while (ss.available() > 0)
    gps.encode(ss.read());
    
  if (gps.location.isUpdated())
  {
    Serial.print(F("LOCATION   Fix Age="));
    Serial.print(gps.location.age());
    Serial.print(F("ms Raw Lat="));
    Serial.print(gps.location.rawLat().negative ? "-" : "+");
    Serial.print(gps.location.rawLat().deg);
    Serial.print("[+");
    Serial.print(gps.location.rawLat().billionths);
    Serial.print(F(" billionths],  Raw Long="));
    Serial.print(gps.location.rawLng().negative ? "-" : "+");
    Serial.print(gps.location.rawLng().deg);
    Serial.print("[+");
    Serial.print(gps.location.rawLng().billionths);
    Serial.print(F(" billionths],  Lat="));
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(" Long="));
    Serial.println(gps.location.lng(), 6);
    flon = gps.location.lng();
    flat = gps.location.lat();
  }

  else if (gps.date.isUpdated())
  {
    Serial.print(F("DATE       Fix Age="));
    Serial.print(gps.date.age());
    Serial.print(F("ms Raw="));
    Serial.print(gps.date.value());
    Serial.print(F(" Year="));
    Serial.print(gps.date.year());
    Serial.print(F(" Month="));
    Serial.print(gps.date.month());
    Serial.print(F(" Day="));
    Serial.println(gps.date.day());
  }

  else if (gps.time.isUpdated())
  {
    Serial.print(F("TIME       Fix Age="));
    Serial.print(gps.time.age());
    Serial.print(F("ms Raw="));
    Serial.print(gps.time.value());
    Serial.print(F(" Hour="));
    Serial.print(gps.time.hour());
    Serial.print(F(" Minute="));
    Serial.print(gps.time.minute());
    Serial.print(F(" Second="));
    Serial.print(gps.time.second());
    Serial.print(F(" Hundredths="));
    Serial.println(gps.time.centisecond());
  }

  else if (gps.speed.isUpdated())
  {
    Serial.print(F("SPEED      Fix Age="));
    Serial.print(gps.speed.age());
    Serial.print(F("ms Raw="));
    Serial.print(gps.speed.value());
    Serial.print(F(" Knots="));
    Serial.print(gps.speed.knots());
    Serial.print(F(" MPH="));
    Serial.print(gps.speed.mph());
    Serial.print(F(" m/s="));
    Serial.print(gps.speed.mps());
    Serial.print(F(" km/h="));
    Serial.println(gps.speed.kmph());
  }

  else if (gps.course.isUpdated())
  {
    Serial.print(F("COURSE     Fix Age="));
    Serial.print(gps.course.age());
    Serial.print(F("ms Raw="));
    Serial.print(gps.course.value());
    Serial.print(F(" Deg="));
    Serial.println(gps.course.deg());
  }

  else if (gps.altitude.isUpdated())
  {
    Serial.print(F("ALTITUDE   Fix Age="));
    Serial.print(gps.altitude.age());
    Serial.print(F("ms Raw="));
    Serial.print(gps.altitude.value());
    Serial.print(F(" Meters="));
    Serial.print(gps.altitude.meters());
    Serial.print(F(" Miles="));
    Serial.print(gps.altitude.miles());
    Serial.print(F(" KM="));
    Serial.print(gps.altitude.kilometers());
    Serial.print(F(" Feet="));
    Serial.println(gps.altitude.feet());
  }

  else if (gps.satellites.isUpdated())
  {
    Serial.print(F("SATELLITES Fix Age="));
    Serial.print(gps.satellites.age());
    Serial.print(F("ms Value="));
    Serial.println(gps.satellites.value());
  }

  else if (gps.hdop.isUpdated())
  {
    Serial.print(F("HDOP       Fix Age="));
    Serial.print(gps.hdop.age());
    Serial.print(F("ms raw="));
    Serial.print(gps.hdop.value());
    Serial.print(F(" hdop="));
    Serial.println(gps.hdop.hdop());
  }

  else if (millis() - last > 5000)
  {
    Serial.println();
    if (gps.location.isValid())
    {
      static const double LONDON_LAT = 51.508131, LONDON_LON = -0.128002;
      double distanceToLondon =
        TinyGPSPlus::distanceBetween(
          gps.location.lat(),
          gps.location.lng(),
          LONDON_LAT, 
          LONDON_LON);
      double courseToLondon =
        TinyGPSPlus::courseTo(
          gps.location.lat(),
          gps.location.lng(),
          LONDON_LAT, 
          LONDON_LON);

      Serial.print(F("LONDON     Distance="));
      Serial.print(distanceToLondon/1000, 6);
      Serial.print(F(" km Course-to="));
      Serial.print(courseToLondon, 6);
      Serial.print(F(" degrees ["));
      Serial.print(TinyGPSPlus::cardinal(courseToLondon));
      Serial.println(F("]"));
    }

    Serial.print(F("DIAGS      Chars="));
    Serial.print(gps.charsProcessed());
    Serial.print(F(" Sentences-with-Fix="));
    Serial.print(gps.sentencesWithFix());
    Serial.print(F(" Failed-checksum="));
    Serial.print(gps.failedChecksum());
    Serial.print(F(" Passed-checksum="));
    Serial.println(gps.passedChecksum());
    
    //Serial.print(F("Soft Serial device overflowed"));
    //Serial.println(ss.overflow() ? "YES!" : "NO");
    Serial.println(gps.location.lng(), 6);
    Serial.println(gps.location.lat(), 6);
    Serial.println(gps.speed.mph());
    Serial.println(gps.time.hour());
    Serial.println(gps.satellites.value());
    Serial.println(gps.location.isValid());
    Serial.println(gps.location.age());
    Serial.println(gps.speed.age());
    Serial.println(gps.satellites.age());

    if (gps.charsProcessed() < 10)
      Serial.println(F("WARNING: No GPS data.  Check wiring."));

    last = millis();
    Serial.println();
  }
}

void readDistance(){
  duration = sonar.ping_median(10); 
  distance = (duration/2) * 0.0343; 
}

void readBattery(){
  voltage = battery.voltage();
  level = battery.level();
}
    
    


void loop(){    
    Serial.print("###########    ");
    Serial.print("COUNT=");
    Serial.print(count);
    Serial.println("    ###########");
     count++;
     //dhtTem();
     //while(gps.satellites.age() > 500)
       GPSread();
     readBattery();
     readDistance();
     dataWrite();
     SendData();
     Serial.println("Starting Sleep");
     delay(100);
     arduinoSleep(8); //In increments of 8 seconds 
     Serial.println("Ending Sleep");

}
