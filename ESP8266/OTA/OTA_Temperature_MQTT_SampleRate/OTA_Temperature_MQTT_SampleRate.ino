/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com
  Arduino IDE example: Examples > Arduino OTA > BasicOTA.ino
*********/


#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <PubSubClient.h>

/*
#include <WiFi.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESPmDNS.h>
*/


// Replace with your network credentials
const char* ssid = "Rowan_IoT";
const char* password = "";

const int ESP_BUILTIN_LED = 2;


#define SEALEVELPRESSURE_HPA (1013.25)

#define ChipID "ESP8266-002" //Name of device

//topic defines//
#define tempTopic ChipID "/temperature"
#define humidTopic ChipID "/humidity"
#define pressTopic ChipID "/pressure"
#define altTopic ChipID "/altitude"
#define sampTopic ChipID "/samplingRate"

//Setting the default sampling rate//
int samplingRate = 5000;

Adafruit_BME280 bme;

float temperature, humidity, pressure, altitude;
char tempString[8];
char humidityString[8];
char pressureString[8];
char altitudeString[8];

//MQTT
const char* mqtt_server = "172.24.130.63";
int mqtt_port = 1883;

WiFiClient espClient;
PubSubClient client(espClient);


//char payload[100];

String payloadString;

void callback(char* topic, byte* payload, unsigned int length){
 Serial.print("Message arrived [");
 Serial.print(topic);
 Serial.print("] ");
 for (int i=0;i<length;i++) {
  char receivedChar = (char)payload[i];
  Serial.print(receivedChar);
  payloadString += (char)payload[i];
  }
  Serial.println();
 if(String(topic) == sampTopic){
  Serial.println("Topics Match: Sampling Rate Changing");
  Serial.print("New Sampling Rate: ");
  Serial.println(String(*payload));
  Serial.println(payloadString.toInt());
  samplingRate = payloadString.toInt();
  payloadString = "";
 }
}


void reconnect() {
 // Loop until we're reconnected
 while (!client.connected()) {
 Serial.print("Attempting MQTT connection...");
 // Attempt to connect
 String clientId = ChipID;
 clientId += String(random(0xffff), HEX);
 if (client.connect(clientId.c_str())) {
  Serial.println("connected");
  // ... and subscribe to topic
  client.subscribe("ledStatus"); //ANY USE IN THIS??
  client.subscribe(sampTopic);
 } else {
  Serial.print("failed, rc=");
  Serial.print(client.state());
  Serial.println(" try again in 5 seconds");
  // Wait 5 seconds before retrying
  delay(5000);
  }
 }
}



void setup() {
  Serial.begin(115200);
  delay(100);  
  bme.begin(0x76);  
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname(ChipID);

  // No authentication by default
  // ArduinoOTA.setPassword((const char *)"123");

  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  pinMode(ESP_BUILTIN_LED, OUTPUT);
  client.setServer(mqtt_server, 1883);  // Defined in secrets.h
  client.setCallback(callback);
}

void loop() {
  ArduinoOTA.handle();
  if(!client.connected()){
    reconnect();
  }
  temperature = bme.readTemperature();
  dtostrf(temperature, 6, 4, tempString);
  humidity = bme.readHumidity();
  dtostrf(humidity, 6, 4, humidityString);
  pressure = bme.readPressure() / 100.0F;
  dtostrf(pressure, 6, 4, pressureString);
  altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);
  dtostrf(altitude, 6, 4, altitudeString);
  client.publish(tempTopic, tempString);
  client.publish(humidTopic, humidityString);
  client.publish(pressTopic, pressureString);
  client.publish(altTopic, altitudeString);
  delay(samplingRate);
  client.loop();
}
