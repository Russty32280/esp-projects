/*
 *  This sketch is a basic implementation of the ESP8266 Wifi Library to connect to a single WiFi SSID.
 *  The SSID and Password is stored in a "secrets.h" file which can be added to a .gitignore file when sharing.
 *  
 *  Modifications (Russell Trafford):
 *  + Implemented secrets.h
 */

#include "secrets.h"
#include <ESP8266WiFi.h>

void setup() {
  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password); // The ssid and password are stored in secrets.h
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}


void loop() {
  Serial.println("Inside the Main Loop");
  delay(1000);
}
