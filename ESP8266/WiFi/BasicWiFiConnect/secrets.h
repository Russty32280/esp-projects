/*
 * Put all of your secrets here. You can put specific information about your wifi config or any MQTT/User information here.
 */

const char* ssid     = "your-ssid";
const char* password = "your-password";
