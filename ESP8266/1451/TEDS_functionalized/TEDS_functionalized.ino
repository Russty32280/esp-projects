/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com
  Arduino IDE example: Examples > Arduino OTA > BasicOTA.ino
*********/


#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <PubSubClient.h>
#include <Ticker.h>



/*
#include <WiFi.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESPmDNS.h>
*/


// Replace with your network credentials
const char* ssid = "Rowan_IoT";
const char* password = "";

const int ESP_BUILTIN_LED = 2;


#define SEALEVELPRESSURE_HPA (1013.25)

#define ChipID "ESP8266-123" //Name of device
String TimID;
#define ESPMac WiFi.macAddress()
#warning
#define TEDS "3"

//topic defines//
#define tempTopic ChipID "/temperature"
#define humidTopic ChipID "/humidity"
#define pressTopic ChipID "/pressure"
#define altTopic ChipID "/altitude"
#define sampTopic ChipID "/samplingRate"

//Setting the default sampling rate//
int samplingRate = 5000;

Adafruit_BME280 bme;

float temperature, humidity, pressure, altitude;
char tempString[8];
char humidityString[8];
char pressureString[8];
char altitudeString[8];

//MQTT
const char* mqtt_server = "172.24.128.29";
int mqtt_port = 1883;

WiFiClient espClient;
PubSubClient client(espClient);



Ticker sampleRateTicker;


//char payload[100];

String payloadString;


void readSensors()
{
  temperature = bme.readTemperature();
  dtostrf(temperature, 6, 4, tempString);
  humidity = bme.readHumidity();
  dtostrf(humidity, 6, 4, humidityString);
  pressure = bme.readPressure() / 100.0F;
  dtostrf(pressure, 6, 4, pressureString);
  altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);
  dtostrf(altitude, 6, 4, altitudeString);
}

void publishSensorData()
{
  client.publish(tempTopic, tempString);
  client.publish(humidTopic, humidityString);
  client.publish(pressTopic, pressureString);
  client.publish(altTopic, altitudeString);
}


void sampleRateCallback()
{
  readSensors();
  publishSensorData();
}

void callback(char* topic, byte* payload, unsigned int length){
 Serial.print("Message arrived [");
 Serial.print(topic);
 Serial.print("] ");
 for (int i=0;i<length;i++) {
  char receivedChar = (char)payload[i];
  Serial.print(receivedChar);
  payloadString += (char)payload[i];
  }
  Serial.println();
 if(String(topic) == sampTopic){
  Serial.println("Topics Match: Sampling Rate Changing");
  Serial.print("New Sampling Rate: ");
  Serial.println(String(*payload));
  Serial.println(payloadString.toInt());
  samplingRate = payloadString.toInt();
  sampleRateTicker.detach();
  sampleRateTicker.attach_ms(samplingRate, sampleRateCallback);
  payloadString = "";
 }
 else if(String(topic) == "NewTim"){
  Serial.println("New Tim Detected assigning ID");
  Serial.print("TimID:");
  Serial.println(payloadString);
  TimID = payloadString;
  Serial.println("unsubscribing from NewTim");
  client.unsubscribe("NewTim");
  client.subscribe((TimID + "/TEDS").c_str());
  Serial.println("Subscribing to:" + TimID + "/TEDS");
 }
 else if(String(topic) == (TimID + "/TEDS")){
  Serial.println(payloadString);
  if(String(payloadString) == "go"){
    Serial.print("publishing number of TEDS to: ");
    Serial.println(TimID + "/TEDS");
    Serial.println("Number of TEDS: "  TEDS); 
    client.publish((TimID + "/TEDS").c_str(), TEDS);
    for(int i=1;i<=String(TEDS).toInt();i++){
      client.publish((TimID + "/Channel/" + i).c_str(), "Hello World");
    }
  }
 }
 payloadString = "";
}


void reconnect() {
 // Loop until we're reconnected
 while (!client.connected()) {
 Serial.print("Attempting MQTT connection...");
 // Attempt to connect
 String clientId = ESPMac;
 clientId += String(random(0xffff), HEX);
 if (client.connect(clientId.c_str())) {
  Serial.println("connected");
  // ... and subscribe to topic
  //client.subscribe("ledStatus"); //ANY USE IN THIS??
  client.subscribe(sampTopic);
  client.publish("NewTim", ESPMac.c_str());
  client.subscribe("NewTim");
 } else {
  Serial.print("failed, rc=");
  Serial.print(client.state());
  Serial.println(" try again in 5 seconds");
  // Wait 5 seconds before retrying
  delay(5000);
  }
 }
}

void WiFiConnect(){
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }
}

void OTAsetup(){
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname(ChipID);

  // No authentication by default
  // ArduinoOTA.setPassword((const char *)"123");

  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup() {
  Serial.begin(115200);
  delay(100);  
  bme.begin(0x76);  
  WiFiConnect();
  OTAsetup();

  pinMode(ESP_BUILTIN_LED, OUTPUT);
  client.setServer(mqtt_server, 1883);  // Defined in secrets.h
  client.setCallback(callback);
  sampleRateTicker.attach_ms(5000, sampleRateCallback);
}

void loop() {
  ArduinoOTA.handle();
  if(!client.connected()){
    reconnect();
  }
  client.loop();
}
