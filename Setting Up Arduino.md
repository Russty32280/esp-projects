# Setting Up Arduino
***
## Downloading the IDE
***
If you don't already have it, download the Arduino IDE, Which can be found [**at this link**](https://learn.sparkfun.com/tutorials/installing-arduino-ide/all "Get Started with Arduino").

## Getting the necessary Libraries and Sketches
***
### Installing Libraries
***
After installing the IDE you will also need some libraries to use the devices and sketches

There are two main ways to install libraries in Arduino

1. Unzip the file and extract it into the libraries directory of the Arduino folder in File Explorer.
	- It will look something like this: C:\Users\YOURUSERNAME\Documents\Arduino\libraries
2. In the IDE go to:  
> Sketch > Include Library > Add .ZIP Library... > Downloads > Select the library you want to include

The following libraries will need to be installed using the method above.

1. [**RadioHead Library**](http://www.airspayce.com/mikem/arduino/RadioHead/RadioHead-1.92.zip "download link")
2. [**PubSubClient**](https://github.com/knolleary/pubsubclient/archive/v2.7.zip "download link")
3. [**TinyGPS++**](https://github.com/mikalhart/TinyGPSPlus/archive/v1.0.2.zip "download link")

Most of the libraries will be installable through the library manager in Arduino by going to Tools > Manage Libraries... > Install the following libraries:

1. Adafruit BME280 Library
2. Adafruit SSD1306 Library
3. Adafruit Unified Sensor
4. ArduinoOTA
5. Battery Sense
6. CayenneLPP
7. DHT sensor library
8. DHT sensor library for ESPx
9. ESP8266 and ESP32 Oled Driver for SSD1306 display
10. EspMQTTClient
11. IBM LMIC framework
12. LoRa
13. MCCI LoRaWAN LMIC library
14. NewPing
15. U8g2
16. WiFiEsp
17. WiFiManager

If there are problems compiling with errors involving library specific items then you are most likely missing a library and it can either be found online or in the built-in library manager.

### Sketches
***
There are a plethora of sketches involving a lot of different devices and sensors.
Luckily, these are all on GITLAB for convenience, note that this is not github but rather gitlab.
You can sign into gitlab with your github account (or atlassian if you prefer bitbucket)
	
The GITLAB is located [**here**](https://gitlab.com/Russty32280/esp-projects "camden project gitlab").
(If you can't access it you probably need to be given access to it which you can do by talking to Russell Trafford)