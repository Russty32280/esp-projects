# ESP Projects

This is a collection of multiple projects all aimed at getting started with the ESP platform and Arduino. Most of these projects are based on example code and other projects found online. Credit is given for each project and its source.

## Recommended Order
While there is an order in this README, most of these projects can be done independently of one another. Projects are currently organized by larger ideas such as WiFi and Over the Air Updates. Below is what order we recommend if you have never dealt with the ESP platform and are looking to explore the WiFi capabilities. 

* WiFi
* ThingSpeak
* MQTT (PubSubClient)
* OTA

