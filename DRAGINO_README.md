# Getting Started With DRAGINO
***
Dragino is the company who manufactures the LoRa Gateway we will be using.
They have a lot of resources that are open source that explain how to use their devices and also give examples.

## The IoT Kit
***
The Dragino kit we have contains the following devices:

1. Dragino LG01 LoRaWAN/WiFi gateway
2. Dragino LoRa shield (mounted on Arduino Uno)
3. Dragino GPS/LoRa shield (also mounted on Arduino Uno)
4. MH Sensor Series Flame Sensor
5. Relay
6. MH Sensor Series Photosensitive Sensor
7. HC-SR04 Ultrasonic Sensor
8. DHT11 Humidity/Temperature Sensor

### Resources
***
1. [Dragino GitHub](https://github.com/dragino/)
2. [Dragino Wiki](https://wiki.dragino.com/index.php?title=Main_Page)
3. [Latest Quick Start Guide](http://www.dragino.com/downloads/index.php?dir=UserManual/)

### Factors to Consider
***
When using any code involving the RadioHead library or anything utilizing LoRa, there are a few things to take into consideration:

1. Frequency (Make sure it's 915.0 MHz)
2. Coding Rate (Usually set to 4/5)
3. Spreading Factor
4. Bandwidth

All of these factors must be consistent between any devices you want to communicate. So if you find that two devices aren't communicating these will be the first factors you will likely want to check.

### Helpful image of how the multiple nodes example works
***
![Concurrent Example Diagram](https://github.com/djstahlberger/stuff/blob/master/Concurrent_Dragino.png)



